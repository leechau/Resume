Li Chao

21 Baiwanzhuang Avenue

Xicheng District

Beijing, China

Mobile: +86 138 1064 7360

E-mail: leechau@126.com

# Profile

* Learning Web development and Meteor for 4 months, building and maintaining [NewFairs](http://newfairs.com/);

* Sophisticated technical mentor and team leader. Teaching interns data mining tools and algorithms for 1 year;

* Translator of a IT technical book, from English to Chinese;

* 4 years of Linux system administration experience;

* 5 years of experience in software development and design, sophisticated in Java and Python;

# Works, Publications and Technical Skills

* Developed and maintaining [DSNote](https://bitbucket.org/leechau/dsnote), a console note-taking application.
  So far I've written more than a thousand notes in English and Chinese with it.
  These notes are created and edited with [markdown](http://daringfireball.net/projects/markdown/) in vim,
  and published on [my github blog](http://leetschau.github.io/) with blogging tool [Octopress](http://octopress.org/);

* Translator of [精通Puppet配置管理工具（第2版）](http://www.ituring.com.cn/book/1300),
  the Chinese version of [Pro Puppet, 2nd edition](http://www.apress.com/9781430260400),
  published in October 2014, by [Posts & Telecom Press](http://www.ptpress.com.cn);

* A fan of vim and Linux shell, using Linux (Mint) as development environment.
  A follower of "Linux and the Unix Philosophy" (by Mike Gancarz).
  This resume is also edited with markdown in vim,
  and converted to pdf with [pandoc](http://johnmacfarlane.net/pandoc/);

* [StackOverflow](http://stackoverflow.com/users/701420/chad) experience: reputation 317, two silver badges and 12 bronze badges;

* Hosting and sharing codes on [GitHub](https://github.com/leetschau) and [Bitbucket](https://bitbucket.org/leechau);

# Career History

* 2002-2004: Product Data Management software engineer in [United Force Corporation](http://ufc.com.cn/);

* 2010-2013: telecom software developer and architect in [Bright Oceans Corporation](http://www.boco.com.cn);

* 2014-present: data mining engineer and team leader in [Bright Oceans Corporation](http://www.boco.com.cn);

# Education

* Ph.D. in Power Machinery Engineering, Beijing Institute of Technology, Jan 2010

* M.S. in Thermodynamic Engineering, Beijing Institute of Technology, Mar 2005

* B.S. in Thermodynamic Engineering. Beijing Institute of Technology, Jul 2002

# Interests

Writing interesting codes and technical blogs, reading books, listening music and pingpong.
Favorite bands: Backstreet Boys, U2.
