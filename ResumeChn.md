### 李超

北京市西城区百万庄大街21号

手机：138 1064 7360

Email: leechau@126.com

期望职位：云数据科学家/大数据平台研发工程师/后端研发工程师

---

# 工作经历

* 2015.5 至今：[牛展网](https://www.newfairs.com/)技术合伙人和主要开发者；

* 2010.5 - 2015.5：[亿阳信通股份有限公司](http://www.boco.com.cn)，数据分析工程师，运维工程师，网管软件开发工程师；

* 2002-2004：[通力有限公司](http://ufc.com.cn/)，产品数据管理软件开发工程师；

---

# 教育背景

* 2010年1月：工学博士，北京理工大学，动力机械工程；

* 2005年3月：工学硕士，北京理工大学，热能与动力工程；

* 2002年7月：工学学士，北京理工大学，热能与动力工程；

---

# 技术特长

## 数据分析

* [告警相关性分析](https://github.com/leetschau/alarm-relations)：
  使用[Apache Spark](http://spark.apache.org/)对网管设备生成的大量告警数据进行相关性分析，
  以提高告警处理效率，降低人工成本；

* [人群移动分析](https://github.com/leetschau/TravelAnalysis)：
  使用Spark和[ArcGIS](https://www.arcgis.com/)基于电信运营商提供的位置数据，
  分析特定时间段内人口的流动特征；

## DevOps

[牛展网](https://www.newfairs.com/)的架构设计和运维：网站包含应用服务、数据库、
搜索服务、支付服务、管理控制台、API服务、内容生成系统、定时服务等微服务组成，其中
应用服务包括多个生成服务（保证高可用）、一个staging版本服务和一个beta版本服务；

目前服务器托管在[青云](https://www.qingcloud.com/)上，
正在基于[Docker](https://www.docker.com/)做容器化转型，
参与了阿里云一个VPN容器的功能改进。

基于[Jenkins](https://jenkins.io/)、[Fabric](http://www.fabfile.org/)以及Python脚本实现自动版本发布和分阶段部署。

版本控制和分支管理使用[git-flow](https://github.com/nvie/gitflow)模型，
任务和文档管理使用[Phabricator](https://www.phacility.com/)。

## Web

[牛展网](https://www.newfairs.com/)使用了Web框架[Meteor](https://www.meteor.com/)，
数据库使用MongoDB，展位图使用SVG矢量图转换后保存，APP服务包含4万多行代码，
其中业务逻辑（JavaScript代码）占66%，内容展现（HTML+JADE代码）占13%，
样式表（LESS+CSS代码）占20%，SVG代码占1%。

## 高并发服务

使用Java NIO框架[Netty](http://netty.io/)和[MINA](https://mina.apache.org/)开发了高并发网管指令分发软件，
支持网元设备间多次Telnet和SSH跳转。

## 脚本语言

主要使用Python和Linux Bash作为运维和日常工作的工具语言，
以开源方式开发了一些配置管理和个人知识管理工具，对提高工作效率发挥了重要作用。

---

# 团队管理

在过去一年多的时间里为牛展网打造了一个小而精的开发团队，
建立了规范的开发、版本发布、部署、任务管理和知识积累流程，
团队成员具备比较强的开发、沟通和学习和能力，在有限的时间和资源下，
完成了网站的原型开发，提供了面向展会参展商（买家）和组委会（卖家）的
在线交易展位（商品）功能。

---

# 各类作品

* GitHub: https://github.com/leetschau

* 技术博客：http://leetschau.github.io/

* StackOverflow: http://stackoverflow.com/users/701420/chad

* 知乎：https://www.zhihu.com/people/leechau

* [精通Puppet配置管理工具](https://book.douban.com/subject/26277133/)译者；

* [Functional Python Programming](https://www.packtpub.com/application-development/functional-python-programming)译者；

# 其他

喜欢追踪数据分析和DevOps领域的新思想、新技术，喜欢Linux Shell的简洁高效。
京东金牌会员，希望为京东的发展贡献一份力量。
